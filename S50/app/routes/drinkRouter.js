//Import thu vien express
const express = require("express");
//Khai bao middleware
const drinkMiddleware = require("../middleware/drinkMiddleware");
//Tao ra router
const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL course: ", req.url);

    next();
});

router.get("/", drinkMiddleware.getAllDrinkMiddleware, (req, res) => {
    res.json({
        message: "Get all drink"
    });
});

router.get("/:drinkId", drinkMiddleware.getADrinkMiddleware, (req, res) => {
    const drinkId = req.params.drinkId;
    res.json({
        message: "Get drink id = " + drinkId
    });
});

router.post("/", drinkMiddleware.createADrinkMiddleware, (req, res) => {
    res.json({
        message: "Get all drink"
    });
});
router.put("/:drinkId", drinkMiddleware.updateADrinkMiddleware, (req, res) => {
    var drinkId = req.params.drinkId;

    res.json({
        message: "Update drink id = " + drinkId
    })
});
router.delete("/:drinkId", drinkMiddleware.deleteADrinkMiddleware, (req, res) => {
    var drinkId = req.params.drinkId;

    res.json({
        message: "Delete drink id = " + drinkId
    })
});

module.exports = router