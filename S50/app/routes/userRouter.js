//Import thu vien express
const express = require("express");
//Khai bao middleware
const userMiddleware = require("../middleware/userMiddleware");
//Tao ra router
const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL course: ", req.url);

    next();
});

router.get("/", userMiddleware.getAllUserMiddleware, (req, res) => {
    res.json({
        message: "Get all User"
    });
});

router.get("/:userId", userMiddleware.getAUserMiddleware, (req, res) => {
    const userId = req.params.userId;
    res.json({
        message: "Get User id = " + userId
    });
});

router.post("/", userMiddleware.createAUserMiddleware, (req, res) => {
    res.json({
        message: "Create a User"
    });
});
router.put("/:userId", userMiddleware.updateAUserMiddleware, (req, res) => {
    var userId = req.params.userId;

    res.json({
        message: "Update User id = " + userId
    })
});
router.delete("/:userId", userMiddleware.deleteAUserMiddleware, (req, res) => {
    var userId = req.params.userId;

    res.json({
        message: "Delete User id = " + userId
    })
});

module.exports = router