//Import thu vien express
const express = require("express");
//Khai bao middleware
const appMiddleware = require("../middleware/appMiddleware");
//Tao ra router
const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL course: ", req.url);

    next();
});

router.get("/", appMiddleware.getAllAppMiddleware, (req, res) => {
    res.json({
        message: "Get all App"
    });
});

router.get("/:AppId", appMiddleware.getAAppMiddleware, (req, res) => {
    const appId = req.params.appId;
    res.json({
        message: "Get App id = " + appId
    });
});

router.post("/", appMiddleware.createAAppMiddleware, (req, res) => {
    res.json({
        message: "Create a App"
    });
});
router.put("/:appId", appMiddleware.updateAAppMiddleware, (req, res) => {
    var appId = req.params.appId;

    res.json({
        message: "Update App id = " + appId
    })
});
router.delete("/:appId", appMiddleware.deleteAAppMiddleware, (req, res) => {
    var appId = req.params.appId;

    res.json({
        message: "Delete App id = " + appId
    })
});

module.exports = router