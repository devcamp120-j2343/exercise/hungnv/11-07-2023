//Import thu vien express
const express = require("express");
//Khai bao middleware
const voucherMiddleware = require("../middleware/voucherMiddleware");
//Tao ra router
const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL course: ", req.url);

    next();
});

router.get("/", voucherMiddleware.getAllVoucherMiddleware, (req, res) => {
    res.json({
        message: "Get all Voucher"
    });
});

router.get("/:voucherId", voucherMiddleware.getAVoucherMiddleware, (req, res) => {
    const voucherId = req.params.voucherId;
    res.json({
        message: "Get Voucher id = " + voucherId
    });
});

router.post("/", voucherMiddleware.createAVoucherMiddleware, (req, res) => {
    res.json({
        message: "Get all Voucher"
    });
});
router.put("/:voucherId", voucherMiddleware.updateAVoucherMiddleware, (req, res) => {
    var voucherId = req.params.voucherId;

    res.json({
        message: "Update Voucher id = " + voucherId
    })
});
router.delete("/:voucherId", voucherMiddleware.deleteAVoucherMiddleware, (req, res) => {
    var voucherId = req.params.voucherId;

    res.json({
        message: "Delete Voucher id = " + voucherId
    })
});

module.exports = router