const getAllAppMiddleware = (req, res, next) => {
    console.log("Get All App!");
    next();
}
const getAAppMiddleware = (req, res, next) => {
    console.log("Get a App!");
    next();
}
const createAAppMiddleware = (req, res, next) => {
    console.log("Create a App!");
    next();
}
const updateAAppMiddleware = (req, res, next) => {
    console.log("Update a App!");
    next();
}
const deleteAAppMiddleware = (req, res, next) => {
    console.log("Delete a App!");
    next();
}

//export
module.exports={
    getAllAppMiddleware,
    getAAppMiddleware,
    createAAppMiddleware,
    updateAAppMiddleware,
    deleteAAppMiddleware
}