const getAllUserMiddleware = (req, res, next) => {
    console.log("Get All User!");
    next();
}
const getAUserMiddleware = (req, res, next) => {
    console.log("Get a User!");
    next();
}
const createAUserMiddleware = (req, res, next) => {
    console.log("Create a User!");
    next();
}
const updateAUserMiddleware = (req, res, next) => {
    console.log("Update a User!");
    next();
}
const deleteAUserMiddleware = (req, res, next) => {
    console.log("Delete a User!");
    next();
}

//export
module.exports={
    getAllUserMiddleware,
    getAUserMiddleware,
    createAUserMiddleware,
    updateAUserMiddleware,
    deleteAUserMiddleware
}