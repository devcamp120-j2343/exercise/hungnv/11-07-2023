const getAllDrinkMiddleware = (req, res, next) => {
    console.log("Get All Drink!");
    next();
}
const getADrinkMiddleware = (req, res, next) => {
    console.log("Get a Drink!");
    next();
}
const createADrinkMiddleware = (req, res, next) => {
    console.log("Create a Drink!");
    next();
}
const updateADrinkMiddleware = (req, res, next) => {
    console.log("Update a Drink!");
    next();
}
const deleteADrinkMiddleware = (req, res, next) => {
    console.log("Delete a Drink!");
    next();
}

//export
module.exports={
    getAllDrinkMiddleware,
    getADrinkMiddleware,
    createADrinkMiddleware,
    updateADrinkMiddleware,
    deleteADrinkMiddleware
}