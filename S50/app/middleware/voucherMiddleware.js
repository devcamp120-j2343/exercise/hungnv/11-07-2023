const getAllVoucherMiddleware = (req, res, next) => {
    console.log("Get All Voucher!");
    next();
}
const getAVoucherMiddleware = (req, res, next) => {
    console.log("Get a Voucher!");
    next();
}
const createAVoucherMiddleware = (req, res, next) => {
    console.log("Create a Voucher!");
    next();
}
const updateAVoucherMiddleware = (req, res, next) => {
    console.log("Update a Voucher!");
    next();
}
const deleteAVoucherMiddleware = (req, res, next) => {
    console.log("Delete a Voucher!");
    next();
}

//export
module.exports={
    getAllVoucherMiddleware,
    getAVoucherMiddleware,
    createAVoucherMiddleware,
    updateAVoucherMiddleware,
    deleteAVoucherMiddleware
}