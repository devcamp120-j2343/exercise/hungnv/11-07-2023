//Import thu vien ExpressJS
//import express form 'express'
const express = require('express');

//khai bao router
const drinkRouter = require('./app/routes/drinkRouter');
const voucherRouter = require('./app/routes/voucherRouter');
const userRouter = require('./app/routes/userRouter');
const appRouter = require('./app/routes/appRouter');
//Khoi tao app node.js bang express
const app = express();

//Khai bao ung dung se chay cong 8000
const port = 8000;

app.use("/api/drink", drinkRouter);
app.use("/api/voucher", voucherRouter);
app.use("/api/user", userRouter);
app.use("/api/app", appRouter);
//Chay ung dung tren cong port:8000
app.listen(port, () => {
    console.log("Ung dung chay tren cong : ", port)
});