//Import thu vien express
const express = require("express");
//Khai bao middleware
const courseMiddleware = require("../middleware/courseMiddleware");
//Tao ra router
const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL course: ", req.url);

    next();
});

router.get("/", courseMiddleware.getAllCourseMiddleware, (req, res) => {
    res.json({
        message: "Get all courses"
    })
})

router.post("/", courseMiddleware.postCourseMiddleware, (req, res) => {
    res.json({
        message: "Create course"
    })
});

router.get("/:courseId", courseMiddleware.getACourseMiddleware, (req, res) => {
    var courseId = req.params.courseId;

    res.json({
        message: "Get course id = " + courseId
    })
});

router.put("/:courseId", courseMiddleware.putCourseMiddleware, (req, res) => {
    var courseId = req.params.courseId;

    res.json({
        message: "Update course id = " + courseId
    })
});

router.delete("/:courseId", courseMiddleware.deleteCourseMiddleware, (req, res) => {
    var courseId = req.params.courseId;

    res.json({
        message: "Delete course id = " + courseId
    })
});

module.exports = router;


