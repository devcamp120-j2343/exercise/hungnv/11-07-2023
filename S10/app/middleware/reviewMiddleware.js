const getAllReviewMiddleware = (req, res, next) => {
    console.log("Get All Review!");
    next();
}

const getAReviewMiddleware = (req, res, next) => {
    console.log("Get a Review!");
    next();
}
const postReviewMiddleware = (req, res, next) => {
    console.log("Create a Review!");
    next();
}

const putReviewMiddleware = (req, res, next) => {
    console.log("Update a Review!");
    next();
}
const deleteReviewMiddleware = (req, res, next) => {
    console.log("Delete a Review!");
    next();
}

//export
module.exports = {
    getAllReviewMiddleware,
    getAReviewMiddleware,
    postReviewMiddleware,
    putReviewMiddleware,
    deleteReviewMiddleware
}