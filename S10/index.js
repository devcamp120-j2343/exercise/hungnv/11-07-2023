//Import thu vien ExpressJS
//import express form 'express'
const express = require('express');

const courseRouter = require("./app/routes/courseRouter");
const reviewRouter = require("./app/routes/reviewRouter");

//Khoi tao app node.js bang express
const app = express();

//Khai bao ung dung se chay cong 8000
const port = 8000;
//middleware log ra thoi gian hien tai 
app.use((req, res, next) => {
    console.log(new Date());
    next();
}, (req, res, next) => {
    console.log(req.method);
    next();
});

//Lay ra ngay thang hom nay
app.get("/", (req, res) => {
    const today = new Date();
    res.status(200).json({
        message: `Hom nay la ngay ${today.getDate()} thang ${today.getMonth() + 1} nam ${today.getFullYear()}`
    })
});

app.use("/api/v1/courses", courseRouter);
app.use("/api/v1/reviews", reviewRouter);

//Chay ung dung tren cong port:8000
app.listen(port, () => {
    console.log("Ung dung chay tren cong : ", port)
});